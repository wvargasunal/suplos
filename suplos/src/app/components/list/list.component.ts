import { Component, HostBinding, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  @HostBinding('class') classes = 'row';
  API_SUPLOS = 'https://pixabay.com/api/?key=13119377-fc7e10c6305a7de49da6ecb25&lang=es&q=';
  producto = [];
  product: string;
  category = ["science", "education", "people", "feelings", "computer", "buildings"];
  seleccionado: string;
  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
    this.getProducts();
  }

  getProducts() {
    this.http.get<any>(`${this.API_SUPLOS}`).subscribe(
      res => {
        console.log(res);
        this.producto = res.hits;
      })
  }
  search(product: string) {
    this.http.get<any>(`${this.API_SUPLOS}` + `${product}`).subscribe(
      res => {
        this.producto = res.hits;
      })
  }
  searchCategory(seleccionado: string) {
    this.http.get<any>(`${this.API_SUPLOS}` + `${seleccionado}`).subscribe(
      res => {
        console.log(res);
        this.producto = res.hits;
      })
  }
}